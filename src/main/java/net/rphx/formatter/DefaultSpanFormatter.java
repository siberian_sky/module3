package net.rphx.formatter;

import net.rphx.formatter.module.TextSpan;
import net.rphx.formatter.module.TextSpanFormattingVisitor;

public class DefaultSpanFormatter implements SpanFormatter {
    private final TextSpanFormattingVisitor<?> formattingVisitor;

    public DefaultSpanFormatter(TextSpanFormattingVisitor<?> formattingVisitor) {
        this.formattingVisitor = formattingVisitor;
    }

    @Override
    public String format(TextSpan span) {
        return span.format(formattingVisitor).toString();
    }
}
