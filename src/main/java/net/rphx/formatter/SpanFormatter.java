package net.rphx.formatter;

import net.rphx.formatter.module.TextSpan;

public interface SpanFormatter {
    String format(TextSpan span);
}