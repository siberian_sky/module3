package net.rphx.formatter.util;

public class HtmlFormatter {
    private HtmlFormatter() {
    }

    public static String formatStrong(String text) {
        return String.format("<strong>%s</strong>", text);
    }

    public static String formatHref(String text) {
        return formatHref(text, text);
    }

    public static String formatHref(String url, String text) {
        return String.format("<a href=\"%s\">%s</a>", url, text);
    }
}
