package net.rphx.formatter;

import net.rphx.formatter.impl.Entity;
import net.rphx.formatter.impl.EntityFormatter;
import net.rphx.formatter.impl.Link;
import net.rphx.formatter.impl.LinkFormatter;
import net.rphx.formatter.impl.ThrowingFormatter;
import net.rphx.formatter.impl.TwitterName;
import net.rphx.formatter.impl.TwitterNameFormatter;
import net.rphx.formatter.module.TextSpan;
import net.rphx.formatter.module.TextSpanFormatter;
import net.rphx.formatter.module.TextSpanFormattingVisitor;

public class DefaultVisitors implements TextSpanFormattingVisitor<String> {
    private final TwitterNameFormatter twitterNameFormatter = new TwitterNameFormatter();
    private final EntityFormatter entityFormatter = new EntityFormatter();
    private final LinkFormatter linkFormatter = new LinkFormatter();

    private final TextSpanFormatter<TextSpan> defaultFormatter = new ThrowingFormatter();

    @Override
    public String format(TextSpan span) {
        return defaultFormatter.format(span);
    }

    @Override
    public String format(Link span) {
        return linkFormatter.format(span);
    }

    @Override
    public String format(Entity span) {
        return entityFormatter.format(span);
    }

    @Override
    public String format(TwitterName span) {
        return twitterNameFormatter.format(span);
    }
}
