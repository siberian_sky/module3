package net.rphx.formatter;

import net.rphx.formatter.module.TextSpan;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class DefaultFormatter implements Formatter {

    private final SpanFormatter spanFormatter;

    public DefaultFormatter(SpanFormatter spanFormatter) {
        this.spanFormatter = spanFormatter;
    }

    public String format(String text, Map<Interval, TextSpan> spans) {
        if (text == null || text.isEmpty() || spans.isEmpty()) {
            return text;
        }
        StringBuilder sb = new StringBuilder();
        TreeMap<Interval, TextSpan> orderedSpans = new TreeMap<>(spans);

        Iterator<Map.Entry<Interval, TextSpan>> iterator =
                orderedSpans.entrySet().iterator();
        int i = 0;
        do {
            Map.Entry<Interval, TextSpan> intervalEntry = iterator.next();
            Interval interval = intervalEntry.getKey();
            int nextInterval = interval.getStart();
            if (nextInterval > i) {
                sb.append(text.substring(i, nextInterval));
            }
            sb.append(spanFormatter.format(intervalEntry.getValue()));
            i = interval.getEnd();
        } while (i < text.length() && iterator.hasNext());
        if (iterator.hasNext()) {
            throw new IllegalStateException("Intervals do not match the text");
        }
        if (i < text.length()) {
            sb.append(text.substring(i));
        }
        return sb.toString();
    }
}
