package net.rphx.formatter.module;

public abstract class BaseTextSpan implements TextSpan {
    private final String text;

    public BaseTextSpan(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return text;
    }
}
