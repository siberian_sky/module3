package net.rphx.formatter.module;

import net.rphx.formatter.impl.Entity;
import net.rphx.formatter.impl.Link;
import net.rphx.formatter.impl.TwitterName;

public interface TextSpanFormattingVisitor<T> {
    T format(TextSpan span);

    T format(Link span);

    T format(Entity span);

    T format(TwitterName span);
}
