package net.rphx.formatter.module;

public interface TextSpan {
    String getText();

    <T> T format(TextSpanFormattingVisitor<T> visitor);
}
