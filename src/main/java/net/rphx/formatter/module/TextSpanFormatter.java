package net.rphx.formatter.module;

public interface TextSpanFormatter<T extends TextSpan> {
    String format(T source);
}
