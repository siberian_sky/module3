package net.rphx.formatter;

import net.rphx.formatter.module.TextSpan;

import java.util.Map;

public interface Formatter {

    String format(String text, Map<Interval, TextSpan> spans);
}
