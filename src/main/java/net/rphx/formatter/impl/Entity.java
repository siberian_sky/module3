package net.rphx.formatter.impl;

import net.rphx.formatter.module.BaseTextSpan;
import net.rphx.formatter.module.TextSpanFormattingVisitor;

public class Entity extends BaseTextSpan{
    public Entity(String text) {
        super(text);
    }

    @Override
    public <T> T format(TextSpanFormattingVisitor<T> visitor) {
        return visitor.format(this);
    }
}
