package net.rphx.formatter.impl;

import net.rphx.formatter.module.TextSpan;
import net.rphx.formatter.module.TextSpanFormatter;

public class ThrowingFormatter implements TextSpanFormatter<TextSpan> {
    @Override
    public String format(TextSpan source) {
        throw new IllegalStateException("No formatter registered for "
                + source.getClass().getSimpleName() + ": " + source.getText());
    }
}
