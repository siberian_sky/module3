package net.rphx.formatter.impl;

import net.rphx.formatter.module.TextSpanFormatter;
import net.rphx.formatter.util.HtmlFormatter;

public class LinkFormatter implements TextSpanFormatter<Link> {
    @Override
    public String format(Link source) {
        return HtmlFormatter.formatHref(source.getText());
    }
}
