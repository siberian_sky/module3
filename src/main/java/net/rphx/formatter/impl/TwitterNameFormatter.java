package net.rphx.formatter.impl;

import net.rphx.formatter.module.TextSpanFormatter;
import net.rphx.formatter.util.HtmlFormatter;

public class TwitterNameFormatter implements TextSpanFormatter<TwitterName> {
    private static final String TWITTER_ADDRESS = "http://twitter.com";

    @Override
    public String format(TwitterName source) {
        return source.getPrefix() +
                HtmlFormatter.formatHref(
                        TWITTER_ADDRESS + "/" + source.getName(),
                        source.getName());
    }
}
