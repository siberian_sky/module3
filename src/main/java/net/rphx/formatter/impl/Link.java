package net.rphx.formatter.impl;

import net.rphx.formatter.module.BaseTextSpan;
import net.rphx.formatter.module.TextSpanFormattingVisitor;

public class Link extends BaseTextSpan {

    public Link(String text) {
        super(text);
    }

    @Override
    public <T> T format(TextSpanFormattingVisitor<T> visitor) {
        return visitor.format(this);
    }
}
