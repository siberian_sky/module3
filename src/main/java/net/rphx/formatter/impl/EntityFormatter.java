package net.rphx.formatter.impl;

import net.rphx.formatter.module.TextSpanFormatter;
import net.rphx.formatter.util.HtmlFormatter;

public class EntityFormatter implements TextSpanFormatter<Entity> {
    @Override
    public String format(Entity source) {
        return HtmlFormatter.formatStrong(source.getText());
    }
}
