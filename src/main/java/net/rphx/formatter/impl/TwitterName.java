package net.rphx.formatter.impl;

import net.rphx.formatter.module.BaseTextSpan;
import net.rphx.formatter.module.TextSpanFormattingVisitor;

public class TwitterName extends BaseTextSpan {
    private final String prefix;
    private final String name;

    public TwitterName(String text) {
        super(text);
        //text is @username
        prefix = text.substring(0, 1);
        name = text.substring(1);
    }

    public String getPrefix() {
        return prefix;
    }

    public String getName() {
        return name;
    }

    @Override
    public <T> T format(TextSpanFormattingVisitor<T> visitor) {
        return visitor.format(this);
    }
}
