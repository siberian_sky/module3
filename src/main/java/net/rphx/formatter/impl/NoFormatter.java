package net.rphx.formatter.impl;

import net.rphx.formatter.module.TextSpan;
import net.rphx.formatter.module.TextSpanFormatter;

public class NoFormatter implements TextSpanFormatter<TextSpan>{
    @Override
    public String format(TextSpan source) {
        return source.getText();
    }
}
