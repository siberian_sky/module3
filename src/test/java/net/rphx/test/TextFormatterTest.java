package net.rphx.test;

import net.rphx.formatter.DefaultFormatter;
import net.rphx.formatter.DefaultSpanFormatter;
import net.rphx.formatter.DefaultVisitors;
import net.rphx.formatter.Formatter;
import net.rphx.formatter.Interval;
import net.rphx.formatter.impl.Entity;
import net.rphx.formatter.impl.Link;
import net.rphx.formatter.impl.TwitterName;
import net.rphx.formatter.module.BaseTextSpan;
import net.rphx.formatter.module.TextSpan;
import net.rphx.formatter.module.TextSpanFormattingVisitor;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TextFormatterTest {

    @Test
    public void testValidInput() {
        String text = "Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile";
        String expectedText = "<strong>Obama</strong> visited <strong>Facebook</strong> headquarters: " +
                "<a href=\"http://bit.ly/xyz\">http://bit.ly/xyz</a> " +
                "@<a href=\"http://twitter.com/elversatile\">elversatile</a>";

        Formatter formatter = new DefaultFormatter(new DefaultSpanFormatter(new DefaultVisitors()));

        Map<Interval, TextSpan> tokens = new HashMap<>();
        tokens.put(new Interval(14, 22), new Entity("Facebook"));
        tokens.put(new Interval(0, 5), new Entity("Obama"));
        tokens.put(new Interval(55, 67), new TwitterName("@elversatile"));
        tokens.put(new Interval(37, 54), new Link("http://bit.ly/xyz"));

        String formatedText = formatter.format(text, tokens);
        assertThat(formatedText, is(expectedText));
    }

    @Test(expected = IllegalStateException.class)
    public void testNewHandler() {
        String text = "#Obama visited Facebook headquarters";
        String expectedText = "<strong>Obama</strong> visited <strong>Facebook</strong> headquarters";

        class Hashtag extends BaseTextSpan {

            Hashtag(String text) {
                super(text);
            }

            @Override
            public <T> T format(TextSpanFormattingVisitor<T> visitor) {
                return visitor.format(this);
            }
        }

        Formatter formatter = new DefaultFormatter(new DefaultSpanFormatter(new DefaultVisitors()));

        Map<Interval, TextSpan> tokens = new HashMap<>();
        tokens.put(new Interval(14, 22), new Entity("Facebook"));
        tokens.put(new Interval(0, 5), new Hashtag("#Obama"));

        String formatedText = formatter.format(text, tokens);
        assertThat(formatedText, is(expectedText));
    }
}
