package net.rphx.test;

import net.rphx.formatter.module.TextSpanFormatter;
import net.rphx.formatter.impl.TwitterName;
import net.rphx.formatter.impl.TwitterNameFormatter;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TwitterFormatterTest {

    @Test
    public void testCorrectInput() {
        TextSpanFormatter<TwitterName> formatter = new TwitterNameFormatter();
        String formatted = formatter.format(new TwitterName("@elversatile"));
        assertThat(formatted, is("@<a href=\"http://twitter.com/elversatile\">elversatile</a>"));
    }
}
